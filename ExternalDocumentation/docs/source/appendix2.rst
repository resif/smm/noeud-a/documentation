
.. _Appendix2:

Appendix 2 : Experiment file
============================

MOMAR.experiment.yaml

.. literalinclude:: MOMAR.experiment.yaml
   :language: yaml
