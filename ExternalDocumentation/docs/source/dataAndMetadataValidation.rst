
.. _VisualValidation:

Data and Metadata Visual Validation
===================================


Visual validation consists of evaluating graphical representations of the data and metadata. The
different types of graphics and which Auditor evaluates them are shown in the :numref:`graphic_types_table`.

.. _graphic_types_table:

.. table:: Types of graphics and auditor
   
   +---------------------------------------------+----------+------------+ 
   |                    Type                     | Facility | Scientific |
   +---------------------------------------------+----------+------------+
   |Station map                                  |  |check| | |check|    |
   +---------------------------------------------+----------+------------+
   |Data availability                            |  |check| | |check|    |
   +---------------------------------------------+----------+------------+
   |Instrument responses                         |  |check| ||uncheck|   |
   +---------------------------------------------+----------+------------+
   |Probabilistic Power Spectral Densities (PPSD)| |uncheck|| |check|    |
   +---------------------------------------------+----------+------------+
   |Waveforms                                    | |uncheck|| |check|    |
   +---------------------------------------------+----------+------------+


.. |check| unicode:: U+2714 .. check sign
.. |uncheck| unicode:: U+2716 .. uncheck sign
   :trim:

An auditor in charge of validation visits the A-Node validation web site via a web browser. This auditor moves through pages, each one presenting a plot. If everything looks correct, he/she can validate the plot. If he/she finds a problem in a plot, he/she writes a description of the problem, and may flag it as an error to
be corrected. 

Access to the validation website
--------------------------------

The validation website is not publicly available. An auditor will receive an email containing a link to the web site. For the security reasons, this link contains a token that will be checked by the site, and there is a limited validity duration.

Facility and Scientific web validation
---------------------------------------

There are 2 kinds of web validation: Facility and Scientific.
The guide for Auditors can be found in the  `Web Validation Guide  <https://web-validation-guide.readthedocs.io/en/latest/index.html>`_.


