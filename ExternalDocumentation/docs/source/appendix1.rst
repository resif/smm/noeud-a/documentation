.. _Appendix1:

Appendix 1 : Terminology and Naming Convention
==============================================

Terminology
-----------

Terms used in the :numref:`NodeA_Overview` and throughout this document are: 

**data**
  Results of instrumentation measurements

**metadata**
  Information on data, networks and experiments

.. _A-Node:
**A-Node**
  Center for the collection, processing and validation of SMM data and metadata, before sending them on to the national Data Center.

.. _DataCenter:
**Epos-France Data Center**
  `Epos-France Data Center  <https://seismology.resif.fr>`_ is
  an open data distribution center using international standards

.. _Facility:
**Facility**
  An instrumentation facility that generates data and metadata

.. _Scientist:
**Scientist**
  The responsable scientist for a given campaign dataset.

.. _ScientificAuditor:
**Scientific Auditor**
  The scientist who will validate a given campaign dataset (may be the same as Scientist)

.. _TechnicalAuditor:
**Technical Auditor**
  The facility representative who will validate a given campaign dataset

Other terms used in this document :

.. _Network:
**Network**
  A set of associated stations. We use the FDSN Network definition. The same network code may be used by more than 1 :ref:`Experiment`.

**Station**
  An instrumentation deployed at a specified position for a specified period

.. _Subnetwork:
**Subnetwork**
  Subset of an FDSN :ref:`Network`, corresponding to a set of Stations whose data are collected by
  a single facility during a single data collection Campaign. 
  A Network may have 1 or more Subnetworks.

.. _Campaign:
**Campaign**
  Subset of :ref:`Experiment`, corresponding to one period of data collection .

.. _Experiment:
**Experiment**
  Scientific program covering data collection for a given experiement.
  May have multiple Campaigns and Subnetworks.

.. _Expedition:
**Expedition**
  A seagoing expedition involved in data collection (deployment, recovery, data collection...). These are
  sometimes referred to as "campaigns" by operators, but never by the A-node, to avoid confusion with data
  collection campaigns, which may cover more than one Expedition.


Naming Convention
-----------------

The references of A-Node names and codes are defined at `the FDSN source identifiers site <http://docs.fdsn.org/projects/source-identifiers/en/v1.0/definition.html>`_ and `the SEED 2.4 manual. <http://docs.fdsn.org/projects/source-identifiers/en/v1.0/definition.html#mapping-of-seed-2-4-codes>`_ All of these codes must be composed of ASCII Uppercase (A-Z) and/or Numeric [0-9]
characters. Station and Location codes may also contain the ASCII Dash (-) character.

Network
^^^^^^^

Network codes contains 1 to 8 characters, but the old standard of 2 characters still dominates and
should be used for software compatibility. Using a 2-character code also allows for automatic upgrade to
the recommended standard for temporary networks: the two digit code plus the 4-character start year (i.e.
4G2007 for the 4G EMSO-Azores network). Network codes must be requested from the FDSN, more information can be found in `the FDSN network site <http://docs.fdsn.org/projects/source-identifiers/en/v1.0/network-codes.html#network-codes>`_

Station
^^^^^^^

A station code uses to identify a station within a network. Station codes may contain 1 to 8 characters. But 1 to 5 character codes, according to `the SEED 2.4 standard <http://docs.fdsn.org/projects/source-identifiers/en/v1.0/definition.html#mapping-of-seed-2-4-codes>`_, are preferable for the software compatibility. If multiple
deployments are made to the same site, one can either use the same station name (if the position does
not change significantly compared with respect to expected traveltimes) or the last character of the station
name can be incremented with each deployment (e.g., "IF12A", "IF12B", "IF12C", ...)

Location
^^^^^^^^

Location codes are mostly used distinguish between channels on a single station that have the same
channel code, or a station whose position is not the same as its channels. Location codes must not exceed
8 characters (`FDSN reference site <http://docs.fdsn.org/projects/source-identifiers/en/v1.0/location-codes.html>`_), but the commonly-used `SEED 2.4 standard <http://docs.fdsn.org/projects/source-identifiers/en/v1.0/definition.html#mapping-of-seed-2-4-codes>`_ allows 0 to 2 characters. We recommend using
"00" for the station and incrementing 2-digit numbers if other locations need to be specified.

Channel
^^^^^^^

The channel code consists of a sequence of three codes : band, source and subsource (ex. SH1, BDH). More information can be found in `the miniseed manual <https://www.fdsn.org/pdf/SEEDManual_V2.4.pdf>`_ and `the reference site for channel code <http://docs.fdsn.org/projects/source-identifiers/en/v1.0/channel-codes.html>`_. 
