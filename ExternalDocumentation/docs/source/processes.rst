Processes
=========

The following data and metadata processings are performed by each entity:

#. Scientist: Experiment metadata
#. Facility: data and subnetwork metadata
#. A-Node: cross-verification data and metadata

.. _Experiment-Processing:

Scientist: Experiment Metadata Processing
-------------------------------------------

The responsible cientist for an experiment provides metadata of this experiment and all of its included campaigns(see section :ref:`scientist-provider` for the file format). If the experiment has multiple campaigns, the scientist adds new campaigns as the experiment progresses and transmits this file to the A-node.

The experiment file is sent via email to smm_anode@services.cnrs.fr. 

.. _Park-Processing:

Facility Processing
------------------- 

OBS facilities collect data during a data collection Campaign. A facility also must have information
(including instrument responses) about its OBSs . These data and metadata are initially processed on the
facility side to obtain basic data (A-node miniSEED) and metadata (SMM StationXML). The Facility then
sends this data to the SMM A-Node using a specific file-transfer protocol.

Facility-level processing consists of :

* Collecting raw data, in a proprietary or standard format.
* Evaluating the raw data for errors, and fixing them if possible, while saving information about any processing/transformation performed.
* Converting the data to A-node miniSEED format (uncorrected for clock drift, if possible)
* Preparing metadata in SMM stationXML format. The obsinfo tool may be used for this.
* Sending data and metadata to the A-Node

Facility Tools
^^^^^^^^^^^^^^

This section presents an example how a facility (INSU-IPGP) prepares metadata and data including some of tools used for this preparation.

* Metadata tools

  - Obsinfo: Create SMM StationXML files using a text-based instrumentation database and relatively clear and compact "subnetwork" files that describe essential information for each station.

* Data tools

  - lcfix: fix some data errors in lcheapo files.
  - lc2ms: transform data from lcheapo format to miniseed format
  - mscat: concatenate miniseed files
  - sdp-process: create/append to a provenance file for each command-line operation (the above commands do this automatically)

* Transfer tool

  - rsync to send data in miniseed format

.. _A-Node-Processing:

A-Node Processing
-----------------

Once the A-Node receives Experiment metadata from the Scientist Provider and data and
metadata from the Facility Provider(s), it :

* pre-validates (validates) the data and metadata
* performs clock correction (drift and possibly leapsecond) on data
* creates visual validation graphics for the Facility and Scientific Auditors, and awaits the validation
* sends the data and metadata to the Epos-France Data Center

A-Node Tools
^^^^^^^^^^^^

The A-Node automatically processes data and metadata, except for visual validation, for which human intervention is required. The sections :ref:`VisualValidation` and :ref:`ValidationGuide` explain this visual validation.
