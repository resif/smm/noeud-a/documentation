---
format_version: "1.0beta"
revision: 
    date: "2020-12-10"
    authors: 
        -
            full_name: "Wayne Crawford"
            email: "crawford@ipgp.fr"
experiment:
    reference_name: "EMSO-MOMAR"
    working_period:
        start_date: "2007-07-20"
        end_date: ~
    fdsn_network:
        code: "4G"
        name: "EMSO-MOMAR"
        description: "Local seismological network around the summit of Lucky Strike volcano."
        start_date: "2007-01-01"
        end_date: "2025-12-31"
    data_distribution:
        embargo_period.a: 3
    campaigns:
        "MOMAR_2007-2008_A":
            scientist:
                full_name: "Wayne Crawford"
                email: "crawford@ipgp.fr"
            collection_period:
                start_date: "2007-07-20"
                end_date: "2008-08-15"
            operated_subnetworks:
                "INSU-IPGP":
                    notes: 
                        - "LSV5A and LSV6A were temporary deployments with currentmeters"
                        - "AZBBA is half-way between Lucky Strike and Ponta Delgado"
                    stations:
                        - 'AZBBA'
                        - 'LSV5A'
                        - 'LSV6A'
                        - 'LSVCA'
                        - 'LSVNA'
                        - 'LSVSA'
                        - 'LSVEA'
                        - 'LSVWA'
            validation_methods:
                BY-WAVEFORM:
                    events:
                        -
                            title: "Local earthquake, early in deployment"
                            notes: ["hand-selected"]
                            occurrence_period:
                                start_time: "2007-08-04T12:32:10Z"
                                duration.s: 30
                        -
                            title: "M0.5 local earthquake"
                            notes: ["from select_local_1km_0709_step2_end.out.xyz"]
                            occurrence_period:
                                start_time: "2007-11-28T03:43:33Z"
                                duration.s: 20
                            source_coordinates: {lat.deg: 37.30741, lon.deg: -32.28497, depth.m: 2413}
                        -
                            title: "M5.3 regional earthquake"
                            notes: ["Recovered from https://earthquake.usgs.gov/earthquakes/search/ using circular region = [37.292N, -32.28E, 200km]"]
                            occurrence_period:
                                start_time: "2008-03-02T00:19:59Z"
                                duration.s: 240
                            source_coordinates: {lat.deg: 36.426, lon.deg: -33.820, depth.m: 10000}
                        -
                            title: "Local earthquake, late in deployment"
                            notes: ["hand-selected"]
                            occurrence_period:
                                start_time: "2008-08-01T10:59:28Z"
                                duration.s: 20
            expedition_actions:
                "BBMOMAR1": ["Deployment"]
                "BBMOMAR2": ["Recovery"]
        "MOMAR_2008-2009_B":
            scientist:
                full_name: "Wayne Crawford"
                email: "crawford@ipgp.fr"
            collection_period:
                start_date: "2008-08-10"
                end_date: "2009-09-02"
            operated_subnetworks:
                "INSU-IPGP":
                    notes: ["Leap second at 2008-12-31T23:59:60"]
                    stations:
                        - 'LSVCB'
                        - 'LSVNB'
                        - 'LSVSB'
                        - 'LSVEB'
                        - 'LSVWB'
            expedition_actions:
                "BBMOMAR2": ["Deployment"]
                "BATHYLUCK2009": ["Recovery"]
            validation_methods:
                BY-WAVEFORM:
                    events:
                        -
                            title: "M1.3 Local earthquake, early in deployment"
                            notes: ["hand-selected"]
                            occurrence_period:
                                start_time: "2008-08-23T06:02:19Z"
                                duration.s: 20
                            source_coordinates: {lat.deg: 37.286, lon.deg: -32.283, depth.m: 2300 }
                        -
                            title: "M1.6 local earthquake"
                            notes: 
                                - "from select_LSFRM_summit_big_clear_2007-2020.inp"
                            occurrence_period:
                                start_time: "2009-03-30T20:27:00Z"
                                duration.s: 20
                            source_coordinates: {lat.deg: 37.288, lon.deg: -32.279, depth.m: 2300 }
                        -
                            title: "Local earthquake, late in deployment"
                            notes: ["hand-selected"]
                            occurrence_period:
                                start_time: "2009-08-30T18:29:57Z"
                                duration.s: 20
        "MOMAR_2009-2010_C":
            scientist:
                full_name: "Wayne Crawford"
                email: "crawford@ipgp.fr"
            collection_period:
                start_date: "2009-09-03"
                end_date: "2010-10-12"
            operated_subnetworks:
                "INSU-IPGP":
                    stations:
                        - 'LSVCC'
                        - 'LSVNC'
                        - 'LSVSC'
                        - 'LSVEC'
                        - 'LSVWC'
            expedition_actions:
                "BATHYLUCK2009": ["Deployment"]
                "MOMARsat2010": ["Recovery"]
            validation_methods:
                BY-WAVEFORM:
                    events:
                        -
                            title: "M1.7 Local earthquake, early in deployment"
                            notes: ["hand-selected"]
                            occurrence_period:
                                start_time: "2009-09-10T06:50:40Z"
                                duration.s: 20
                            source_coordinates: {lat.deg: 37.159, lon.deg: -32.378, depth.m: 4500 }
                        -
                            title: "M1.3 local earthquake"
                            notes:  ["from select_LSFRM_summit_big_clear_2007-2020.inp"]
                            occurrence_period:
                                start_time: "2010-01-07T04:58:57Z"
                                duration.s: 20
                            source_coordinates: {lat.deg: 37.284, lon.deg: -32.277, depth.m: 3400 }
                        -
                            title: "M1.1 Local earthquake, late in deployment"
                            notes: ["hand-selected"]
                            occurrence_period:
                                start_time: "2010-09-26T00:39:10Z"
                                duration.s: 20
        "MOMAR_2010-2011_D":
            scientist:
                full_name: "Wayne Crawford"
                email: "crawford@ipgp.fr"
            collection_period:
                start_date: "2010-10-04"
                end_date: "2011-07-07"
            operated_subnetworks:
                "INSU-IPGP":
                    stations:
                        - 'LSVCD'
                        - 'LSVND'
                        - 'LSVSD'
                        - 'LSVED'
                        - 'LSVWD'
            expedition_actions:
                "MOMARsat2010": ["Deployment"]
                "MOMARsat2011": ["Recovery"]
            validation_methods:
                BY-WAVEFORM:
                    events:
                        -
                            title: "M1.4 Local earthquake, early in deployment"
                            notes: ["hand-selected"]
                            occurrence_period:
                                start_time: "2010-10-30T00:25:30Z"
                                duration.s: 20
                        -
                            title: "M0.7 local earthquake, late in deployment"
                            notes: ["from select_LSFRM_summit_big_clear_2007-2020.inp"]
                            occurrence_period:
                                start_time: "2011-06-14T03:27:20Z"
                                duration.s: 20
                            source_coordinates: {lat.deg: 37.299, lon.deg: -32.286, depth.m: 2500 }
        "MOMAR_2011-2012_E":
            scientist:
                full_name: "Wayne Crawford"
                email: "crawford@ipgp.fr"
            collection_period:
                start_date: "2011-07-11"
                end_date: "2012-07-17"
            operated_subnetworks:
                "INSU-IPGP":
                    notes: ["Leap second on 2012-06-31T23:59:60"]
                    stations:
                        - 'LSVCE'
                        - 'LSVNE'
                        - 'LSVSE'
                        - 'LSVEE'
                        - 'LSVWE'
            validation_methods:
                BY-WAVEFORM:
                    events:
                        -
                            title: "M0.8 local earthquake, early in deployment"
                            notes: ["hand-selected"]
                            occurrence_period:
                                start_time: "2011-07-15T15:28:24Z"
                                duration.s: 15
                            source_coordinates: {lat.deg: 37.282, lon.deg: -32.285, depth.m: 3200}
                        -
                            title: "M0.5 local earthquake"
                            notes: ["from select_LSFRM_summit_big_clear_2007-2020.inp"]
                            occurrence_period:
                                start_time: "2011-10-19T14:02:37Z"
                                duration.s: 20
                            source_coordinates: {lat.deg: 37.289, lon.deg: -32.289, depth.m: 2700}
                        -
                            title: "Local earthquake, late in deployment"
                            notes: ["hand-selected"]
                            occurrence_period:
                                start_time: "2012-07-01T17:46:00Z"
                                duration.s: 20
            expedition_actions:
                "MOMARsat2011": ["Deployment"]
                "MOMARsat2012": ["Recovery"]
        "MOMAR_2012-2013_F":
            scientist:
                full_name: "Wayne Crawford"
                email: "crawford@ipgp.fr"
            collection_period:
                start_date: "2012-07-18"
                end_date: "2013-08-29"
            operated_subnetworks:
                "INSU-IPGP":
                    stations:
                        - 'LSVCF'
                        - 'LSVNF'
                        - 'LSVSF'
                        - 'LSVEF'
                        - 'LSVWF'
            validation_methods:
                BY-WAVEFORM:
                    events:
                        -
                            title: "M2.4 Regional earthquake, early in deployment"
                            notes: ["hand-selected"]
                            occurrence_period:
                                start_time: "2012-07-23T21:01:45Z"
                                duration.s: 20
                            source_coordinates: {lat.deg: 37.414, lon.deg: -32.058, depth.m: 20000}
                        -
                            title: "M0.5 local earthquake"
                            notes: ["from select_LSFRM_summit_big_clear_2007-2020.inp"]
                            occurrence_period:
                                start_time: "2012-10-19T05:51:28Z"
                                duration.s: 20
                            source_coordinates: {lat.deg: 37.289, lon.deg: -32.289, depth.m: 2700}
                        -
                            title: "M2.1 Regional earthquake, late in deployment"
                            notes: ["hand-selected"]
                            occurrence_period:
                                start_time: "2013-08-20T14:46:15Z"
                                duration.s: 20
                            source_coordinates: {lat.deg: 37.101, lon.deg: -32.512, depth.m: 4500}
            expedition_actions:
                "MOMARsat2012": ["Deployment"]
                "MOMARsat2013": ["Recovery"]
        "MOMAR_2013-2014_G":
            scientist:
                full_name: "Wayne Crawford"
                email: "crawford@ipgp.fr"
            collection_period:
                start_date: "2013-09-07"
                end_date: "2013-09-07"
            operated_subnetworks:
                "INSU-IPGP":
                    stations: []
            validation_methods:
                BY-WAVEFORM:
                    events: []
            expedition_actions:
                "MOMARsat2013": ["Deployment"]
                "MOMARsat2014": ["Recovery"]
            notes: ["All stations failed. Sara Hussni + Wayne deployment"]
        "MOMAR_2014-2015_H":
            scientist:
                full_name: "Wayne Crawford"
                email: "crawford@ipgp.fr"
            collection_period:
                start_date: "2014-07-20"
                end_date: "2015-04-19"
            operated_subnetworks:
                "INSU-IPGP":
                    stations:
                        - 'LSVSH'
                        - 'LSVEH'
                        - 'LSVWH'
            notes: ["stations LSVNH et LSVCH n'ont pas marché"]
            validation_methods:
                BY-WAVEFORM:
                    events:
                        -
                            title: "Local earthquake, early in deployment"
                            notes: ["hand-scanned"]
                            occurrence_period:
                                start_time: "2014-07-24T20:40:30Z"
                                duration.s: 20
                        -
                            title: "local? earthquake, late in deployment"
                            notes: ["hand-scanned"]
                            occurrence_period:
                                start_time: "2015-04-01T23:30:06Z"
                                duration.s: 20
            expedition_actions:
                "MOMARsat2014": ["Deployment"]
                "MOMARsat2015": ["Recovery"]
        "MOMAR_2015-2016_I":
            scientist:
                full_name: "Wayne Crawford"
                email: "crawford@ipgp.fr"
            collection_period:
                start_date: "2015-04-19"
                end_date: "2016-05-28"
            operated_subnetworks:
                "INSU-IPGP":
                    stations:
                        - 'LSVCI'
                        - 'LSVNI'
                        - 'LSVSI'
                        - 'LSVEI'
                        - 'LSVWI'
            notes: ["Leapsecond at 2015-06-30:23:59:60"]
            validation_methods:
                BY-WAVEFORM:
                    events:
                        -
                            title: "Local earthquake, early in deployment"
                            notes: ["hand-scanned"]
                            occurrence_period:
                                start_time: "2015-04-24T15:23:18Z"
                                duration.s: 20
                            source_coordinates: {lat.deg: 37.290, lon.deg: -32.278, depth.m: 2900}
                        -
                            title: "M0.5 local earthquake"
                            notes: ["from select_LSFRM_summit_big_clear_2007-2020.inp"]
                            occurrence_period:
                                start_time: "2015-09-26T22:54:48Z"
                                duration.s: 20
                            source_coordinates: {lat.deg: 37.282, lon.deg: -32.285, depth.m: 1500}
                        -
                            title: "Regional earthquake, late in deployment"
                            notes: ["hand-scanned"]
                            occurrence_period:
                                start_time: "2016-05-16T01:22:50Z"
                                duration.s: 20
                            source_coordinates: {lat.deg: 37.432, lon.deg: -32.031, depth.m: 3900}
            expedition_actions:
                "MOMARsat2015": ["Deployment"]
                "Archipelago2016": ["Recovery stations autonomes"]
                "MOMARsat2016": ["Recovery station central"]
        "MOMAR_2016-2017_J":
            scientist:
                full_name: "Wayne Crawford"
                email: "crawford@ipgp.fr"
            collection_period:
                start_date: "2016-08-31"
                end_date: "2017-07-13"
            operated_subnetworks:
                "INSU-IPGP":
                     stations:
                        - 'LSVCJ'
                        - 'LSVHJ'
            notes:
                - "Only central station and HydrOctopus deployed"
                - "No HydrOctopus info in network file"
                - "Central station is bad at end"
            validation_methods:
                BY-WAVEFORM:
                    events:
                       -
                            title: "Local earthquake, early in deployment"
                            notes: ["hand-scanned"]
                            occurrence_period:
                                start_time: "2016-09-12T00:23:40Z"
                                duration.s: 20
                       -
                            title: "Local earthquake, late in deployment"
                            notes: ["hand-scanned"]
                            occurrence_period:
                                start_time: "2017-07-10T00:25:10Z"
                                duration.s: 20
            expedition_actions:
                "MOMARsat2016": ["Deployment"]
                "MOMARsat2017": ["Recovery"]
        "MOMAR_2017-2018_K":
            scientist:
                full_name: "Wayne Crawford"
                email: "crawford@ipgp.fr"
            collection_period:
                start_date: "2017-07-14"
                end_date: "2018-08-13"
            operated_subnetworks:
                "INSU-IPGP":
                    stations:
                        - 'LSVCK'
                        - 'LSVNK'
                        - 'LSVSK'
                        - 'LSVEK'
                        - 'LSVWK'
            validation_methods:
                BY-WAVEFORM:
                    events:
                       -
                            title: "Regional earthquake, early in deployment"
                            notes: ["hand-scanned"]
                            occurrence_period:
                                start_time: "2017-07-26T15:07:24Z"
                                duration.s: 20
                       -
                            title: "Regional earthquake during LSVEK data period"
                            notes: ["hand-scanned"]
                            occurrence_period:
                                start_time: "2017-09-12T14:30:35Z"
                                duration.s: 30
                       -
                            title: "Regional earthquake, late in deployment"
                            notes: ["hand-scanned"]
                            occurrence_period:
                                start_time: "2018-07-01T07:36:05Z"
                                duration.s: 25
            expedition_actions:
                "MOMARsat2017": ["Deployment"]
                "MOMARsat2018": ["Recovery"]
        "MOMAR_2018-2019_L":
            scientist:
                full_name: "Wayne Crawford"
                email: "crawford@ipgp.fr"
            collection_period:
                start_date: "2018-08-13"
                end_date: "2019-06-14"
            notes:
                - "LSVCL is from 2018-09-13 to 2019-01-01"
                - "LSVEL is from 2019-02-04 to end"
                - "LSVNL is cut at 2018-12-11"
            operated_subnetworks:
                "INSU-IPGP":
                    stations:
                        - 'LSVCL'
                        - 'LSVNL'
                        - 'LSVSL'
                        - 'LSVEL'
                        - 'LSVWL'
            validation_methods:
                BY-WAVEFORM:
                    events:
                       -
                            title: "Regional earthquake, early in deployment"
                            notes: ["hand-scanned"]
                            occurrence_period:
                                start_time: "2018-08-20T00:32:45Z"
                                duration.s: 25
                       -
                            title: "Regional earthquake after LSVCL starts"
                            notes: ["hand-scanned"]
                            occurrence_period:
                                start_time: "2018-09-15T07:28:45Z"
                                duration.s: 30
                       -
                            title: "Regional earthquake, late in deployment"
                            notes: ["hand-scanned"]
                            occurrence_period:
                                start_time: "2019-06-06T07:15:50Z"
                                duration.s: 35
            expedition_actions:
                "MOMARsat2018": ["Deployment"]
                "MOMARsat2019": ["Recovery"]
        "MOMAR_2019-2020_M":
            scientist:
                full_name: "Wayne Crawford"
                email: "crawford@ipgp.fr"
            collection_period:
                start_date: "2019-06-20"
                end_date: "2020-09-17"
            operated_subnetworks:
                "INSU-IPGP":
                    stations:
                        - 'LSVCM'
            validation_methods: {
                "BY-WAVEFORM": {"events": []}}
            expedition_actions:
                "MOMARsat2019": ["Deployment"]
                "MOMARsat2020": ["Recovery"]
        "MOMAR_2020-2021_N":
            scientist:
                full_name: "Wayne Crawford"
                email: "crawford@ipgp.fr"
            collection_period:
                start_date: "2020-09-20"
                end_date: "2021-05-30"
            operated_subnetworks:
                "INSU-IPGP":
                    stations:
                        - 'LSVCN'
            validation_methods: {
                "BY-WAVEFORM": {"events": []}}
            expedition_actions:
                "MOMARsat2020": ["Deployment"]
                "MOMARsat2021": ["Recovery"]
        "MOMAR_2021-2022_O":
            scientist:
                full_name: "Wayne Crawford"
                email: "crawford@ipgp.fr"
            collection_period:
                start_date: "2018-08-13"
                end_date: "2022-06-11"
            operated_subnetworks:
                "INSU-IPGP":
                    stations:
                        - 'LSVNO'
                        - 'LSVSO'
                        - 'LSVEO'
                        - 'LSVWO'
            validation_methods: {
                "BY-WAVEFORM": {"events": []}}
            expedition_actions:
                "MOMARsat2021": ["Deployment"]
                "MOMARsat2022": ["Recovery"]
        "MOMAR_2022-2023_P":
            scientist:
                full_name: "Wayne Crawford"
                email: "crawford@ipgp.fr"
            collection_period:
                start_date: "2022-06-10"
                end_date: "2019-06-14"
            operated_subnetworks:
                "INSU-IPGP":
                    stations:
                        - 'LSVCP'
                        - 'LSVNP'
                        - 'LSVSP'
                        - 'LSVEP'
                        - 'LSVWP'
            validation_methods: {
                "BY-WAVEFORM": {"events": []}}
            expedition_actions:
                "MOMARsat2022": ["Deployment"]
                "MOMARsat2023": ["Recovery"]
    expeditions:
        "BBMOMAR1":
            ship_name: "N/O Suroit"
            navigation_period: {start_date: "2007-07-18", end_date: "2007-07-27"}
        "BBMOMAR2":
            ship_name: "N/O Suroit"
            navigation_period: {start_date: "2008-08-08", end_date: "2008-08-17"}
        "BATHYLUCK2009":
            ship_name: "N/O Pourquoi Pas?"
            navigation_period: {start_date: "2009-08-31", end_date: "2009-09-29"}
        "MOMARsat2010":
            ship_name: "N/O Pourquoi Pas?"
            navigation_period: {start_date: "2010-10-02", end_date: "2010-10-16"}
        "MOMARsat2011":
            ship_name: "N/O Pourquoi Pas?"
            navigation_period: {start_date: "2011-06-28", end_date: "2011-07-23"}
        "MOMARsat2012":
            ship_name: "N/O Thalassa"
            navigation_period: {start_date: "2012-07-10", end_date: "2012-07-27"}
        "MOMARsat2013":
            ship_name: "N/O Pourquoi Pas?"
            navigation_period: {start_date: "2013-08-23", end_date: "2013-09-07"}
        "MOMARsat2014":
            ship_name: "N/O Pourquoi Pas?"
            navigation_period: {start_date: "2014-07-13", end_date: "2014-07-31"}
        "MOMARsat2015":
            ship_name: "N/O Pourquoi Pas?"
            navigation_period: {start_date: "2015-04-08", end_date: "2015-04-29"}
        "Archipelago2016":
            ship_name: "N/O Archipelago"
            navigation_period: {start_date: "2016-05-27", end_date: "2016-05-29"}
            participation: {full_names: ["Wayne Crawford", "Charles Poitou"]}
        "MOMARsat2016":
            ship_name: "N/O Atalante"
            navigation_period: {start_date: "2016-08-24", end_date: "2016-09-13"}
        "MOMARsat2017":
            ship_name: "N/O Pourquoi Pas?"
            navigation_period: {start_date: "2017-07-08", end_date: "2017-07-28"}
        "MOMARsat2018":
            ship_name: "N/O Atalante"
            navigation_period: {start_date: "2018-08-08", end_date: "2018-08-27"}
        "MOMARsat2019":
            ship_name: "N/O Pourquoi Pas?"
            navigation_period: {start_date: "2019-06-11", end_date: "2019-07-03"}
        "MOMARsat2020":
            ship_name: "N/O Atalante"
            navigation_period: {start_date: "2020-05-15", end_date: "2020-06-21"}
        "MOMARsat2021":
            ship_name: "N/O Pourquoi Pas?"
            navigation_period: {start_date: "2021-09-03", end_date: "2021-10-09"}
        "MOMARsat2022":
            ship_name: "N/O Pourquoi Pas?"
            navigation_period: {start_date: "2022-06-06", end_date: "2022-06-27"}
            participation: {full_names: ["Tom Dumouch", "Fabrice Fontaine", "Mathilde Cannat"]}
        "MOMARsat2023":
            ship_name: "N/O Atalante"
            navigation_period: {start_date: "2023-07-11", end_date: "2023-07-29"}
