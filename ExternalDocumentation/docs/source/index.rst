.. A-Node documentation master file

A-Node documentation
===================

Table of Contents
-----------------

.. toctree::
   :numbered:

   overview
   dataAndMetadataProvider
   processes
   dataAndMetadataValidation
   
.. toctree::
   
   appendix1
   appendix2
   appendix3
