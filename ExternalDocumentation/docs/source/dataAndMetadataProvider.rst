Providers
=========

The A-Node collects :ref:`Campaign <Campaign>` metadata from a :ref:`Scientist <Scientist>` :ref:`Provider <Provider>` and data and metadata from
a :ref:`Facility <Facility>` :ref:`Provider <Provider>`. This section describes the format of each and how to transmit them.

.. _scientist-provider:
 
Scientist-Provided Metadata
---------------------------

The :ref:`Scientist <Scientist>` :ref:`Provider <Provider>` provides an ``experiment`` metadata file, including metadata for the current
:ref:`Campaign <Campaign>`


Metadata content and formats
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The experiment metadata file provides cross-checking information on facilities, station names and the FDSN
code, as well as time windows to plot in order to validate station timing and channel functioning/amplitudes. To
evaluate how channel quality and timing evolve during the experiments, one time window should be near the
beginning of the data collection Campagn and one near the end.

Optional "expedition" elements are not used by the A-node, but may be useful to the scientist for recording what
ships and personnel were used for each Campaign, as well as the actions performed.

The following is an example `experiment.yaml` file for a single campaign, with one subnetwork of two stations.

.. code-block:: yaml

   ---
   format_version: "1.0beta"
   revision: 
      date: "2020-12-10"
      authors: 
         -
            full_name: "Wayne Crawford"
            email: "crawford@ipgp.fr"
   experiment:
      reference_name: "EMSO-MOMAR"
      working_period:
        start_date: 2007-07-20
        end_date: ~
      fdsn_network:
        code: "4G"
        name: "EMSO-MOMAR"
        description: "Local seismological network around the summit of Lucky Strike volcano."
        start_date: "2007-01-01"
        end_date: "2025-12-31"
      data_distribution:
        embargo_period.a: 3
      campaigns:
        "MOMAR_2007-2008_A":
            scientist:
                full_name: "Wayne Crawford"
                email: "crawford@ipgp.fr"
            collection_period:
                start_date: "2007-07-20"
                end_date: "2008-08-15"
            operated_subnetworks:
                "INSU-IPGP":
                    notes: 
                        - "LSV5A and LSV6A were temporary deployments with currentmeters"
                        - "AZBBA is half-way between Lucky Strike and Ponta Delgado"
                    stations:
                        - 'LSVCA'
                        - 'LSVNA'
            validation_methods:
                BY-WAVEFORM:
                    events:
                        -
                            title: "Local earthquake, early in deployment"
                            notes: ["hand-selected"]
                            occurrence_period:
                                start_time: "2007-08-04T12:32:10"
                                duration.s: 30
                        -
                            title: "M5.3 regional earthquake"
                            notes: ["Recovered from https://earthquake.usgs.gov/earthquakes/search/ using circular region = [37.292N, -32.28E, 200km]"]
                            occurrence_period:
                                start_time: "2008-03-02T00:19:59"
                                duration.s: 240
                            source_coordinates: {lat.deg: 36.426, lon.deg: -33.820, depth.m: 10000}
                        -
                            title: "Local earthquake, late in deployment"
                            notes: ["hand-selected"]
                            occurrence_period:
                                start_time: "2008-08-01T10:59:28"
                                duration.s: 20
 


:ref:`Appendix2` contains a complete example of an experiment file.

Some examples of experiment files dan be found on `the Mobile Marine Experiment Information site <https://gitlab.com/resif/smm/smminfo/-/tree/main/file_formats/experiment/samples?ref_type=heads>`_.
How this experiment file will be used later, can be found in the section :ref:`Experiment-Processing`.

File name
^^^^^^^^^

The preferred experiment file name is 

<EXPERIMENT_REFERENCE_NAME>.experiment.yaml 

For example :

MOMAR.experiment.yaml

Experiment file local validation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Before sending an experiment file to A-Node, you should validate it. This section explains how to validate an experiment file online using two websites and the reference schema.

1. Download the experiment schema file smminfo-experiment.schema.json from `the Mobile Marine Experiment site <https://gitlab.com/resif/smm/smminfo/-/blob/main/file_formats/experiment/schemas/smminfo-experiment.schema.json>`_.

2. Convert your experiment file to JSON format, using the site https://jsonformatter.org/yaml-to-json 

3. Validate the experiment file against the schema, by copying these files into their respective text bodes at https://www.jsonschemavalidator.net to , 


.. _parks_metadata_provider:

Facility-Provided Metadata
---------------------------


Metadata content and formats
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Subnetwork metadata should be provided to the A-Node in `StationXML <http://docs.fdsn.org/projects/stationxml/en/latest/>`_
format. 
Besides the standard StationXML fields (see the FDSN StationXML Reference), the A-Node needs some additional processing information, particularly for correcting the instrument clock. Since there is not appropriate StationXML
field for this, the time correction must be added as a Comment. For example: 

.. code-block:: XML

      <Comment>
        <Value>{"clock_correction_linear": {"time_base": "Seascan MCXO, ~1e-8 nominal drift", "reference": "GPS", "start_sync_instrument": 0, "start_sync_reference": "2007-07-20T18:07:00Z", "end_sync_reference": "2007-07-23T19:09:59.990Z", "end_sync_instrument": "2007-07-23T19:10:00Z"}}</Value>
      </Comment>

For details on the format of the drift correction comment, as well as a possible leapsecond correction
comment, see the FDSN document on Mobile Marie Seismology data/metadata standards.
     
:ref:`Appendix3` contains a partial example of a stationXML file.

Metadata tools
^^^^^^^^^^^^^^

The section :ref:`Park-Processing` shows how a facility can prepare this metadata using `obsinfo <htthttps://gitlab.com/resif/smm/obsinfo>`_ software package. 
Obsinfo automatically injects the appropriate comments for clock drift and leapseconds.

Some other tools mentionned in the `fdsn <https://docs.fdsn.org/projects/stationxml/en/latest/tools.html>`_ site allowing the creation of a stationXML file.

File name
^^^^^^^^^

The StationXML file should be named 

<NETWORK>.<CAMPAIGN>.<OPERATOR_NAME>.station.xml

For example,

4G.MOMAR_2007-2008_A.INSU-IPGP.station.xml


.. _parks_data_provider:

Facility-Provided Data
----------------------

Data format and tools
^^^^^^^^^^^^^^^^^^^^^^^^

The A-Node expects data `miniseed <http://www.fdsn.org/pdf/SEEDManual_V2.4.pdf>`_ format, with one file per station-channel-location.

The section :ref:`Park-Processing` shows un example of data preparation at the INSU-IPGP facility.

File name
^^^^^^^^^

The miniseed file naming convention is:

<NETWORK>.<STATION>.<LOCATION>.<CHANNEL>.<YEAR>.<JDATE>.<HHMMSS>.mseed

where <YEAR>.<JDATE>.<HHMMSS> represents the YEAR, JULIAN DATE, HOUR, MINUTE and SECOND of the start sample of the miniseed data

Example :

4G.AZBBA.00.BH1.2007.207.180000.mseed


