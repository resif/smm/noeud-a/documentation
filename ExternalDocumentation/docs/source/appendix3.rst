
.. _Appendix3:

Appendix 3 : StationXML file
============================

MOMAR_2007-2008_A.INSU-IPGP.station.xml

.. literalinclude:: MOMAR_2007-2008_A.INSU-IPGP.station.xml
   :language: xml
