.. _Overview:

Overview
========


In order to distribute mobile marine seismology (hereafter "SMM" for its French acronym) data to the
scientific community, these data and their associated metadata must be packaged in standard formats
and validated. The SMM A-Node helps instrumentation facilities and experiment scientists to work
together to deliver and validate these data.

The basic processing unit is a "Campaign", which covers one cycle of data collection. Basic data and
meta data are prepared by the involved facilities, then sent to the A-Node A for further processing and
validation. After validation, the A-node sends the data and metadata to the Data Center.

This documentation provides the information necessary for Facilities and Scientists to transmit the
necessary data/metadata to the SMM A-node and to validate the results.

Figure :numref:`NodeA_Overview`  shows the relationships between the SMM A-Node,
instrumentation facilities and campaign scientists. 

 .. _NodeA_Overview:
 .. figure:: images/NodeA_Overview.png
   :scale: 80%
   
   Relationship between the SMM A-Node, Facilities, Scientists and the Epos-France Data Center
   
More information on terminology and naming convention can be found in :ref:`Appendix1`

Users and Roles
---------------

Figure :numref:`NodeA_Overview` shows 2 categories of Users and 2 categories of Roles with respect to the A-node.  They are:

Users
    :ref:`Scientist <Scientist>`: Providers of experiment metadata and scientific validation.

    :ref:`Facility`: Providers of data, subnetwork metadata and technical validation.

Roles
    Provider
      Furnishes data (Technical) and metadata (Technical and Scientific)
    
    Auditor
      Validates the data/metadata consolidated by the A-Node

for a given User, the same person may have both Provider and Auditor.


Data and Metadata Processing
----------------------------

Processing flow
^^^^^^^^^^^^^^^

Data and metadata are processed :ref:`Subnetwork` by :ref:`Subnetwork`. Figure :numref:`label2`  of
shows which data/metadata are prepared by :ref:`Scientist` and :ref:`Facility` :ref:`Provider`  before
sending to the A-Node.

 .. _label2:
 .. figure:: images/ProcessingFlow_En.png
   :scale: 80%

   Processing Flow
   
A :ref:`Facility` :ref:`Provider` collects raw data from instrumentation, transforms them to "A-node" miniseed, and prepares metadata
for each station.  For more information see :ref:`parks_data_provider` and :ref:`parks_metadata_provider`. 

A :ref:`Scientist` :ref:`Provider`  writes an Experiment metadata file, which covers all :ref:`SubNetwork`s and
is updated at each data collection :ref:`Campaign`.
For more information, see  :ref:`scientist-provider`.

The A-Node receives this data and metadata, rectifies and transforms them, then prepares validation pages
for the :ref:`Facilty` and :ref:`Scientist`. More information for the technical auditor is at :ref:`technical-web` and for the scientific auditor at :ref:`scientific-web`.


Processes and Tools
^^^^^^^^^^^^^^^^^^^

Each processing step is performed by at least one specific tool, and the details should be saved in a
provenance file. 
The tools used may be different from facility to facility, see :ref:`Experiment-Processing` and :ref:`Park-Processing`. Below are the steps performed at the A-node.

Rectification
*************

Before processing the data, the A-Node validates the consistency between data and metadata such as the network code, the number of stations and the station codes. It also validates the consistency between the :ref:`Facility`-provided Network information and that recorded by the `FDSN  <https://www.fdsn.org/networks/>`_. Further explanation can be found in the :ref:`NodeA-Processing` section


